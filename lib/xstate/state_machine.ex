defmodule Xstate.StateMachine do
  use Norm
  import Xstate.TransitionRunner

  @doc """

  transits a state machine from a state using an event

  ## Examples

      iex> machine = %{id: "test_a_b",initial: :a,context: %{},states: %{a: %{on: %{MOVE: :b}},b: %{}}}
      iex> Xstate.StateMachine.transition(machine, :a, :MOVE)
      {:ok, %{context: %{}, state: :b}}
      iex> Xstate.StateMachine.transition(machine, :a, {:MOVE, %{}})
      {:ok, %{context: %{}, state: :b}}

      iex> machine = %{id: "test_missing_state",initial: :a,context: %{},states: %{a: %{on: %{MOVE: :b}},b: %{}}}
      iex> Xstate.StateMachine.transition(machine, :not_existing, :MOVE)
      {
        :error,
        %{
          function: :validate_state,
          message: "State not found :not_existing",
          value: %{event: %{name: :MOVE}, machine: %{context: %{}, id: "test_missing_state", initial: :not_existing, states: %{a: %{on: %{MOVE: :b}}, b: %{}}}}
        }
      }

      iex> machine = %{id: "test_missing_event",initial: :a,context: %{},states: %{a: %{on: %{MOVE: :b}},b: %{}}}
      iex> Xstate.StateMachine.transition(machine, :a, :NOT_EXISTING)
      {
        :error,
        %{
          function: :validate_event_exists,
          message: "For state :a event not found :NOT_EXISTING",
          value: %{
            event: %{name: :NOT_EXISTING},
            machine: %{
              context: %{},
              id: "test_missing_event",
              initial: :a,
              states: %{a: %{on: %{MOVE: :b}}, b: %{}}
            }
          }
        }
      }

  """
  @contract transition(
              machine :: state_machine_schema(),
              from ::
                one_of([
                  spec(is_atom()),
                  selection(schema(%{state: spec(is_atom()), context: spec(is_map())}))
                ]),
              event :: one_of([spec(is_atom()), {spec(is_atom()), spec(is_map())}])
            ) :: transition_response_schema()
  def transition(machine, from, event) do
    %{machine: machine}
    |> initialize_state(from)
    |> initialize_context(from)
    |> initialize_event(event)
    |> validate_state
    |> validate_event_exists
    |> validate_event_target_exists
    |> apply_actions
    |> apply_transition
    |> collect_result
  end

  # norm

  @doc """

  verifies that a state machine conforms to the expected schema

  ## Examples

      iex> machine = %{id: "test_a_b",initial: :a,context: %{},states: %{a: %{on: %{MOVE: :b}},b: %{on: %{CANCEL: :a}}}}
      iex> Norm.conform!(machine, Xstate.StateMachine.state_machine_schema())
      %{id: "test_a_b",initial: :a,context: %{},states: %{a: %{on: %{MOVE: :b}},b: %{on: %{CANCEL: :a}}}}

  """
  def state_machine_schema() do
    selection(
      schema(%{
        id: spec(is_binary()),
        initial: spec(is_atom()),
        context: spec(is_map()),
        states:
          map_of(
            spec(is_atom()),
            selection(
              schema(%{
                on:
                  map_of(
                    spec(is_atom()),
                    one_of([
                      spec(is_atom()),
                      selection(
                        schema(%{
                          actions: spec(is_atom()),
                          target: spec(is_atom())
                        })
                      )
                    ])
                  )
              })
            )
          ),
        actions: spec(is_map())
      }),
      [:states]
    )
  end

  @doc """

  verifies that a state machine transition response conforms to the expected schema

  ## Examples


      iex> Norm.conform!({:ok, %{context: %{}, state: :b}}, Xstate.StateMachine.transition_response_schema())
      {:ok, %{context: %{}, state: :b}}

  """
  def transition_response_schema() do
    one_of([
      {:ok,
       selection(
         schema(%{
           context: spec(is_map()),
           state: spec(is_atom())
         })
       )},
      {:error,
       schema(%{
         function: spec(is_atom()),
         message: spec(is_binary()),
         value:
           schema(%{
             event: schema(%{name: spec(is_atom())}),
             machine: state_machine_schema()
           })
       })}
    ])
  end
end
