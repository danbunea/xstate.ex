defmodule Xstate.EventMapper do
  @doc ~S"""

  Transforms the string keys in a map, to atoms, recursively

  ## Examples

      iex> Xstate.EventMapper.atom_map(%{"c"=> "d", "x" => %{"yy" => "zz"},"ls"=>[%{"ls1"=>1}]})
      %{c: "d", x: %{yy: "zz"}, ls: [%{ls1: 1}]}
  """
  def atom_map(map), do: to_atom_map(map)

  defp to_atom_map(map) when is_map(map),
    do: Map.new(map, fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)

  defp to_atom_map(ls) when is_list(ls), do: Enum.map(ls, &to_atom_map(&1))
  defp to_atom_map(v), do: v

  @doc ~S"""

  returns the integer value from a string or the value i not possible

  ## Examples

      iex> Xstate.EventMapper.cast_integer("1")
      1
      iex> Xstate.EventMapper.cast_integer("a1")
      "a1"
      iex> Xstate.EventMapper.cast_integer(%{a: 1})
      %{a: 1}

  """
  def cast_integer(str) when is_binary(str) do
    case Integer.parse(str) do
      :error -> str
      {value, ""} -> value
      {_value, _error} -> str
    end
  end

  def cast_integer(non_str) do
    non_str
  end

  @doc ~S"""

  returns the integer value from a string or the value i not possible

  ## Examples

      iex> Xstate.EventMapper.cast_nilable_integer("")
      nil
      iex> Xstate.EventMapper.cast_nilable_integer(" ")
      nil
      iex> Xstate.EventMapper.cast_nilable_integer("1")
      1
      iex> Xstate.EventMapper.cast_nilable_integer("a1")
      nil
      iex> Xstate.EventMapper.cast_nilable_integer("aa")
      nil
  """
  def cast_nilable_integer(""), do: nil

  def cast_nilable_integer(str) when is_binary(str) do
    case Integer.parse(str) do
      :error -> nil
      {value, ""} -> value
      {_, _} -> nil
    end
  end

  @doc """

  updates a value in the map using a function

  ## Examples
      iex> Xstate.EventMapper.update_values(%{c: "1", x: %{yy: "zz"}, z: "2"}, [:c], &Xstate.EventMapper.cast_integer(&1))
      %{c: 1, x: %{yy: "zz"}, z: "2"}

  """
  def update_values(map, list, func) do
    Map.new(
      map,
      fn {k, v} ->
        {
          k,
          if Enum.member?(list, k) do
            func.(v)
          else
            v
          end
        }
      end
    )
  end
end
