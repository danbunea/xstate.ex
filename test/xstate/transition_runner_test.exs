defmodule Xstate.TransitionRunnerTest do
  use ExUnit.Case
  doctest Xstate.TransitionRunner
  alias Xstate.TransitionRunner

  @machine %{
    id: "test_a_b",
    initial: :a,
    context: %{},
    states: %{
      a: %{
        on: %{
          MOVE: :b,
          INVALID_EVENT: :invalid_state
        }
      },
      b: %{on: %{MOVE: :a}},
      c: %{
        on: %{
          EVENT_WITH_ACTIONS: %{
            target: :a,
            actions: :action_1
          },
          EVENT_WITH_ACTIONS_OK: %{
            target: :a,
            actions: :action_1_ok
          },
          EVENT_WITH_ACTIONS_ERROR: %{
            target: :a,
            actions: :action_1_error
          }
        }
      }
    },
    actions: %{
      action_1: {__MODULE__, :action_1},
      action_1_ok: {__MODULE__, :action_1_ok},
      action_1_error: {__MODULE__, :action_1_error}
    }
  }

  def action_1(ctx, %{done: done, user_id: user_id}) do
    ctx
    |> Map.put(:done, done)
    |> Map.put(:user_id, user_id)
  end

  def action_1(ctx, _event) do
    Map.put(ctx, :done, true)
  end

  def action_1_ok(ctx, _event) do
    {:ok, Map.put(ctx, :done, true)}
  end

  def action_1_error(_ctx, _event) do
    {:error, %{errors: ["Failed"]}}
  end

  describe "transition runner should: " do
    test "initialize state with atom" do
      value = %{machine: @machine}
      expected = {:ok, put_in(value, [:machine, :initial], :a)}
      assert expected == TransitionRunner.initialize_state(value, :a)
    end

    test "initialize state with map" do
      value = %{machine: @machine}
      expected = {:ok, put_in(value, [:machine, :initial], :a)}
      assert expected == TransitionRunner.initialize_state(value, %{state: :a})
    end

    test "fail to initialize state with map with no state" do
      value = %{machine: @machine}

      assert {
               :error,
               %{
                 function: :initialize_state,
                 message: "Missing :state key in from map",
                 value: value
               }
             } ==
               TransitionRunner.initialize_state(value, %{})
    end

    test "initialize context when none sent" do
      value = %{machine: @machine}

      expected = {
        :ok,
        value
        |> put_in([:machine, :initial], :a)
        |> put_in([:machine, :context], %{})
      }

      assert expected == TransitionRunner.initialize_state(value, :a)
    end

    test "initialize context when none sent as map" do
      value = %{machine: @machine}

      expected = {
        :ok,
        value
        |> put_in([:machine, :initial], :a)
        |> put_in([:machine, :context], %{})
      }

      assert expected == TransitionRunner.initialize_context({:ok, value}, %{state: :a})
    end

    test "initialize context when sent" do
      context = %{sent: true}
      value = %{machine: @machine}

      expected = {
        :ok,
        value
        |> put_in([:machine, :initial], :a)
        |> put_in([:machine, :context], context)
      }

      assert expected ==
               TransitionRunner.initialize_context({:ok, value}, %{state: :a, context: context})
    end

    test "initialize event when atom" do
      value = %{machine: @machine}

      expected = {
        :ok,
        value
        |> put_in([:machine, :initial], :a)
        |> put_in([:event], %{name: :EVENT})
      }

      assert expected == TransitionRunner.initialize_event({:ok, value}, :EVENT)
    end

    test "initialize event when tuple" do
      value = %{machine: @machine}

      expected = {
        :ok,
        value
        |> put_in([:machine, :initial], :a)
        |> put_in([:event], %{name: :EVENT, user_id: 5})
      }

      assert expected == TransitionRunner.initialize_event({:ok, value}, {:EVENT, %{user_id: 5}})
    end

    test "validate state" do
      value = {
        :ok,
        %{
          machine: @machine
        }
      }

      assert value == TransitionRunner.validate_state(value)
    end

    test "fail to validate when not finding state" do
      value = %{
        machine: put_in(@machine, [:initial], :non_existing)
      }

      assert {
               :error,
               %{
                 function: :validate_state,
                 message: "State not found :non_existing",
                 value: value
               }
             } == TransitionRunner.validate_state({:ok, value})
    end

    test "validate event in current_state" do
      value =
        %{machine: @machine}
        |> put_in([:machine, :initial], :a)
        |> put_in([:event], %{name: :MOVE, user_id: 5})

      expected = {
        :ok,
        value
        |> put_in([:event, :target], :b)
      }

      assert expected == TransitionRunner.validate_event_exists({:ok, value})
    end

    test "fail to validate when not finding event in current state" do
      value =
        %{machine: @machine}
        |> put_in([:machine, :initial], :a)
        |> put_in([:event], %{name: :NON_EXISTING_EVENT})

      assert {
               :error,
               %{
                 function: :validate_event_exists,
                 message: "For state :a event not found :NON_EXISTING_EVENT",
                 value: value
               }
             } ==
               TransitionRunner.validate_event_exists({:ok, value})
    end

    test "validation error not finding event target" do
      value =
        %{machine: @machine}
        |> put_in([:machine, :initial], :a)
        |> put_in(
          [:event],
          %{
            target: :invalid_state,
            name: :INVALID_EVENT
          }
        )

      assert {
               :error,
               %{
                 function: :validate_event_exists,
                 message:
                   "In test_a_b for state :a on event :INVALID_EVENT target :invalid_state not found",
                 value: value
               }
             } ==
               TransitionRunner.validate_event_target_exists({:ok, value})
    end

    test "apply actions with {module, action}" do
      value = %{
        machine:
          @machine
          |> put_in([:initial], :c),
        event: %{
          target: :a,
          name: :EVENT_WITH_ACTIONS,
          actions: @machine.states.c.on[:EVENT_WITH_ACTIONS].actions
        }
      }

      expected = {
        :ok,
        value
        |> put_in([:machine, :context], %{done: true})
      }

      assert expected == TransitionRunner.apply_actions({:ok, value})
    end

    test "apply actions with anonymous fn" do
      value = %{
        machine:
          @machine
          |> put_in([:initial], :c)
          |> put_in(
            [:actions, :action_1],
            fn ctx, _ev ->
              Map.put(ctx, :done, true)
            end
          ),
        event: %{
          target: :a,
          name: :EVENT_WITH_ACTIONS,
          actions: @machine.states.c.on[:EVENT_WITH_ACTIONS].actions
        }
      }

      expected = {
        :ok,
        value
        |> put_in([:machine, :context], %{done: true})
      }

      assert expected == TransitionRunner.apply_actions({:ok, value})
    end

    test "apply actions with {module, action} returns {:ok, _}" do
      value = %{
        machine:
          @machine
          |> put_in([:initial], :c),
        event: %{
          target: :a,
          name: :EVENT_WITH_ACTIONS_OK,
          actions: @machine.states.c.on[:EVENT_WITH_ACTIONS_OK].actions
        }
      }

      expected = {
        :ok,
        value
        |> put_in([:machine, :context], %{done: true})
      }

      assert expected == TransitionRunner.apply_actions({:ok, value})
    end

    test "fail to apply actions with {module, action} returns {:error, _}" do
      value = %{
        machine:
          @machine
          |> put_in([:initial], :c),
        event: %{
          target: :a,
          name: :EVENT_WITH_ACTIONS_ERROR,
          actions: @machine.states.c.on[:EVENT_WITH_ACTIONS_ERROR].actions
        }
      }

      expected = {
        :error,
        %{errors: ["Failed"]}
      }

      assert expected == TransitionRunner.apply_actions({:ok, value})
    end

    test "apply_transition" do
      value = %{
        machine:
          @machine
          |> put_in([:initial], :a),
        event: %{
          target: :c
        }
      }

      expected = {
        :ok,
        value.machine
        |> put_in([:value], :c)
      }

      assert expected == TransitionRunner.apply_transition({:ok, value})
    end

    test "collect results" do
      value =
        @machine
        |> put_in([:initial], :a)
        |> put_in([:value], :b)

      assert {:ok, %{state: :b, context: %{}}} == TransitionRunner.collect_result({:ok, value})
    end
  end
end
