defmodule Xstate.EventMapperTest do
  use ExUnit.Case
  doctest Xstate.EventMapper

  alias Xstate.EventMapper, as: EM

  describe "event mapper should: " do
    test "make all keys atoms" do
      assert %{c: "d", x: %{yy: "zz"}} == EM.atom_map(%{"c" => "d", "x" => %{"yy" => "zz"}})
    end

    test "parse strings to integer" do
      assert 1 == EM.cast_integer("1")
    end

    test "fail to parse strings to integer" do
      assert "1a" == EM.cast_integer("1a")
      assert %{a: 1} == EM.cast_integer(%{a: 1})
    end

    test "parse strings to nilable integer" do
      assert 1 == EM.cast_nilable_integer("1")
      assert nil == EM.cast_nilable_integer("1a")
      assert nil == EM.cast_nilable_integer("nil")
      assert nil == EM.cast_nilable_integer("")
    end

    test "update selected values, to integer" do
      assert %{c: 1, x: %{yy: "zz"}, z: "2"} ==
               EM.update_values(%{c: "1", x: %{yy: "zz"}, z: "2"}, [:c], &EM.cast_integer(&1))
    end
  end
end
