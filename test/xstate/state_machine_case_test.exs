defmodule Xstate.StateMachineTestHelpersTest do
  use Xstate.StateMachineCase

  describe "state machine test helper should: " do
    @machine %{
      id: "test_a_b",
      initial: :a,
      context: %{},
      states: %{
        a: %{
          on: %{
            MOVE: :b,
            INVALID_EVENT: :invalid_state
          }
        },
        b: %{on: %{}},
        c: %{
          on: %{
            EVENT_WITH_ACTIONS: %{
              target: :a,
              actions: :action_1
            }
          }
        }
      },
      actions: %{
        action_1: {Xstate.StateMachineTest, :action_1}
      }
    }

    test "allow entire machines to be tested using assert_machine" do
      expectations = %{
        a: %{
          MOVE: :b
        },
        c: %{
          EVENT_WITH_ACTIONS: {:action_1, :a}
        }
      }

      assert_machine(expectations, @machine)
    end
  end
end
