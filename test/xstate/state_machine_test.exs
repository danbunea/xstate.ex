defmodule Xstate.StateMachineTest do
  use ExUnit.Case
  doctest Xstate.StateMachine
  alias Xstate.StateMachine

  @machine %{
    id: "test_a_b",
    initial: :a,
    context: %{},
    states: %{
      a: %{
        on: %{
          MOVE: :b,
          INVALID_EVENT: :invalid_state
        }
      },
      b: %{on: %{MOVE: :a}},
      c: %{
        on: %{
          EVENT_WITH_ACTIONS: %{
            target: :a,
            actions: :action_1
          },
          EVENT_WITH_ACTIONS_OK: %{
            target: :a,
            actions: :action_1_ok
          },
          EVENT_WITH_ACTIONS_ERROR: %{
            target: :a,
            actions: :action_1_error
          }
        }
      }
    },
    actions: %{
      action_1: {__MODULE__, :action_1},
      action_1_ok: {__MODULE__, :action_1_ok},
      action_1_error: {__MODULE__, :action_1_error}
    }
  }

  def action_1(ctx, %{done: done, user_id: user_id}) do
    ctx
    |> Map.put(:done, done)
    |> Map.put(:user_id, user_id)
  end

  def action_1(ctx, _event) do
    Map.put(ctx, :done, true)
  end

  def action_1_ok(ctx, _event) do
    {:ok, Map.put(ctx, :done, true)}
  end

  def action_1_error(_ctx, _event) do
    {:error, %{errors: ["Failed"]}}
  end

  describe "state machine (transition) should: " do
    test "transition from one state to another, with empty context and no actions" do
      assert {:ok, %{state: :b, context: %{}}} == StateMachine.transition(@machine, :a, :MOVE)
    end

    test "transition from one state to another, with context and no actions" do
      context = %{a: 1}

      assert {:ok, %{state: :b, context: context}} ==
               StateMachine.transition(
                 @machine,
                 %{state: :a, context: context},
                 :MOVE
               )
    end

    test "fail to transition from one state to another, with empty context and no actions" do
      {:error, %{message: message}} =
        StateMachine.transition(
          @machine,
          :b,
          :NON_EXISTING_EVENT
        )

      assert "For state :b event not found :NON_EXISTING_EVENT" ==
               message
    end

    test "fail to find a state in the state machine" do
      {:error, %{message: message}} =
        StateMachine.transition(
          @machine,
          :no_state,
          :no_event
        )

      assert "State not found :no_state" == message
    end

    test "transition from one state to another, with context and action" do
      assert {
               :ok,
               %{
                 state: :a,
                 context: %{
                   done: true
                 }
               }
             } ==
               StateMachine.transition(
                 @machine,
                 :c,
                 :EVENT_WITH_ACTIONS
               )
    end

    test "fail to transition on error, from one state to another, with context and action" do
      assert {
               :error,
               %{
                 errors: ["Failed"]
               }
             } ==
               StateMachine.transition(
                 @machine,
                 :c,
                 :EVENT_WITH_ACTIONS_ERROR
               )
    end

    test "transition from one state to another, with context and action and event parameters" do
      assert {
               :ok,
               %{
                 state: :a,
                 context: %{
                   done: true,
                   user_id: 1
                 }
               }
             } ==
               StateMachine.transition(
                 @machine,
                 %{
                   state: :c,
                   context: %{
                     done: false
                   }
                 },
                 {:EVENT_WITH_ACTIONS, %{done: true, user_id: 1}}
               )
    end

    test "transition from one state to another, with context and action and event parameters on {:ok, ctx}" do
      assert {
               :ok,
               %{
                 state: :a,
                 context: %{
                   done: true,
                   user_id: 1
                 }
               }
             } ==
               StateMachine.transition(
                 @machine,
                 %{
                   state: :c,
                   context: %{
                     done: false,
                     user_id: 1
                   }
                 },
                 {:EVENT_WITH_ACTIONS_OK, %{done: true, user_id: 1}}
               )
    end

    test "fail to transition on error, from one state to another, with context and action and event parameters" do
      assert {
               :error,
               %{
                 errors: ["Failed"]
               }
             } ==
               StateMachine.transition(
                 @machine,
                 %{
                   state: :c,
                   context: %{
                     done: false
                   }
                 },
                 {:EVENT_WITH_ACTIONS_ERROR, %{done: true, user_id: 1}}
               )
    end
  end
end
