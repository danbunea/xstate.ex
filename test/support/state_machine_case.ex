defmodule Xstate.StateMachineCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Xstate.StateMachine

      def capture_action_invoked(action, ctx, ev) do
        Map.put(ctx, :invoked, action)
      end

      def assert_machine(expectations, machine) do
        for {from_state, events} <- expectations do
          for {event, event_result} <- events do
            case event_result do
              {expected_action, expected_to_state} ->
                test_machine =
                  put_in(
                    machine,
                    [:actions, expected_action],
                    &capture_action_invoked(expected_action, &1, &2)
                  )

                case StateMachine.transition(test_machine, from_state, event) do
                  {:ok, %{state: actual_to_state, context: context}} ->
                    assert expected_to_state == actual_to_state
                    assert expected_action == context.invoked

                  {:error, error} ->
                    assert expected_to_state == error
                end

              expected_to_state when is_atom(expected_to_state) ->
                case StateMachine.transition(machine, from_state, event) do
                  {:ok, %{state: actual_to_state, context: context}} ->
                    assert expected_to_state == actual_to_state

                  {:error, error} ->
                    assert expected_to_state == error
                end
            end
          end
        end
      end
    end
  end
end
